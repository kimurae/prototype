class Prototype

  # This allows one to set new attributes so long as you
  # do so using '='.
  def method_missing(meth, *args, &block)
    if meth.to_s =~ /^.+=$/
      if args[0].kind_of?(Proc)
        self.singleton_class.send(:define_method, meth.to_s.gsub(/=/,'').to_sym, *args)
      else 
        self.singleton_class.send(:attr_accessor,meth.to_s.gsub(/=/,'').to_sym)
        self.send(meth, *args, &block)
      end
    else
      super
    end
  end

  # Modified to agree with method_missing.
  def respond_to?(meth, include_private = false)
    meth.to_s =~ /^.+=$/ || super
  end


end
