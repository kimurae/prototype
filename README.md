# Prototype

A fun class that creates a Prototype object where you can just add methods and attributes
willy-nilly.

## Synopsis

```ruby

p = Prototype.new

p.respond_to?(:"x=") # <-- true
p.respond_to?(:"x")  # <-- false

p.x = 2

p.f = Proc.new do |y|
  self.x + y
end

p.f(2) # <-- 4

```

## Installation
 
Checkout this code from github and install it as a gem (for now).

## Contributing
Fork the project, create an issue and make a pull request.

## Acknowledgments 
Jerome's random banter.
