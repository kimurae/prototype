$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "prototype/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "prototype"
  s.version     = Prototype::VERSION
  s.authors     = ['Jason Kenney']
  s.email       = ['bloodycelt@gmail.com']
  s.homepage    = "https://github.com/bloodycelt"
  s.summary     = "Prototype Object that inspired by Jerome."
  s.description = "Javascript-like prototypes."

  s.files = [
    'MIT-LICENSE', 
    'Rakefile',
    'README.md',
    'lib/prototype.rb'
  ]
  
  s.test_files = [
    'spec/prototype_spec.rb',
    'spec/spec_helper.rb'
  ]

  s.add_development_dependency 'rspec'
  
end
