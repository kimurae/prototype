require 'spec_helper'
require 'prototype'

describe Prototype do

  it "should allow adding attributes" do
    p   = Prototype.new 
    p.x = 2
    
    p.x.should equal(2)

  end

  it 'should respond to anything with an "="' do

    p = Prototype.new
    
    p.should be_respond_to(:"x=")

  end

  it 'should create new methods if a function is passed as an attribute' do

    p = Prototype.new

    p.f = Proc.new { |y|
      self.x + y
    }

    p.x = 2

    p.should be_respond_to(:f)
    p.f(2).should eq(4)

  end

  it 'should only create methods for the instance, not the class' do

    p = Prototype.new

    p.f = Proc.new { 2 }

    p.should be_respond_to(:f)
    Prototype.new.should_not be_respond_to(:f)
  
  end

end
